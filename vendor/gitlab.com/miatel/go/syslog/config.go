package syslog

type config struct {
	priority Priority
	tag      string
	hostname string
	network  string
	raddr    string
}

type Option = func(*config)

func WithPriority(priority Priority) Option {
	return func(v *config) { v.priority = priority }
}

func WithTag(tag string) Option {
	return func(v *config) { v.tag = tag }
}

func WithNetwork(network string) Option {
	return func(v *config) { v.network = network }
}

func WithRemoteAddress(raddr string) Option {
	return func(v *config) { v.raddr = raddr }
}
