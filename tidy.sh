#!/bin/sh

go get -u
go mod tidy
go mod vendor

git add -A vendor
