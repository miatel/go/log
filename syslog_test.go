package log_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/miatel/go/log"
)

func TestSyslogLogger(t *testing.T) {
	RunSuite(t, func() (log.Logger, error) {
		return log.NewDefaultSyslogLogger("tag")
	})
}

func BenchmarkSyslogLogger(b *testing.B) {
	logger, err := log.NewDefaultSyslogLogger("tag")
	require.NoError(b, err)

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		logger.Print("test")
	}
}
