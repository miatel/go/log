package log

import (
	"fmt"
	"sync/atomic"
	"time"
)

type EmptyLogger struct {
	level  atomic.Uint32
	prefix string
	fields Fields
}

func NewDefaultEmptyLogger() (*EmptyLogger, error) {
	logger := &EmptyLogger{
		prefix: "main",
	}
	logger.SetLevel(InfoLevel)

	return logger, nil
}

func (l *EmptyLogger) Close() error {
	return nil
}

func (l *EmptyLogger) Print(...any) {}

func (l *EmptyLogger) Trace(...any) {}

func (l *EmptyLogger) Debug(...any) {}

func (l *EmptyLogger) Info(...any) {}

func (l *EmptyLogger) Notice(...any) {}

func (l *EmptyLogger) Warn(...any) {}

func (l *EmptyLogger) Error(...any) {}

func (l *EmptyLogger) Fatal(...any) {}

func (l *EmptyLogger) Panic(args ...any) {
	panic(fmt.Sprint(args...))
}

func (l *EmptyLogger) Printf(string, ...any) {}

func (l *EmptyLogger) Tracef(string, ...any) {}

func (l *EmptyLogger) Debugf(string, ...any) {}

func (l *EmptyLogger) Infof(string, ...any) {}

func (l *EmptyLogger) Noticef(string, ...any) {}

func (l *EmptyLogger) Warnf(string, ...any) {}

func (l *EmptyLogger) Errorf(string, ...any) {}

func (l *EmptyLogger) Fatalf(string, ...any) {}

func (l *EmptyLogger) Panicf(format string, args ...any) {
	panic(fmt.Sprintf(format, args...))
}

func (l *EmptyLogger) WithPrefix(prefix string) Logger {
	logger := &EmptyLogger{
		prefix: prefix,
		fields: l.fields,
	}
	logger.SetLevel(l.GetLevel())

	return logger
}

func (l *EmptyLogger) Prefix() string {
	return l.prefix
}

func (l *EmptyLogger) WithFields(fields Fields) Logger {
	logger := &EmptyLogger{
		prefix: l.prefix,
		fields: l.fields.WithFields(fields),
	}
	logger.SetLevel(l.GetLevel())

	return logger
}

func (l *EmptyLogger) Fields() Fields {
	return l.fields
}

func (l *EmptyLogger) Elapsed(start time.Time) Logger {
	return l.WithFields(Fields{"elapsed": time.Since(start).String()})
}

func (l *EmptyLogger) GetLevel() Level {
	return Level(l.level.Load())
}

func (l *EmptyLogger) SetLevel(level Level) Logger {
	if level > TraceLevel {
		level = TraceLevel
	}

	l.level.Store(uint32(level))

	return l
}
