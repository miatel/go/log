package log_test

import (
	"testing"

	"gitlab.com/miatel/go/log"
)

func TestEmptyLogger(t *testing.T) {
	RunSuite(t, func() (log.Logger, error) {
		return log.NewDefaultEmptyLogger()
	})
}
