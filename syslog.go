package log

import (
	"fmt"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/miatel/go/syslog"
)

const maxBufSize = 4096

type SyslogLogger struct {
	level       atomic.Uint32
	prefix      string
	fields      Fields
	fieldsCache string
	pool        *sync.Pool
	writer      *syslog.Writer
}

func NewDefaultSyslogLogger(tag string) (logger *SyslogLogger, err error) {
	logger = &SyslogLogger{
		prefix: "main",
		pool: &sync.Pool{
			New: func() interface{} {
				buf := make([]byte, 0, maxBufSize)
				return &buf
			},
		},
	}
	logger.SetLevel(InfoLevel)
	if logger.writer, err = syslog.New(
		syslog.WithPriority(syslog.LOG_DEBUG|syslog.LOG_LOCAL0),
		syslog.WithTag(strings.ToLower(tag)),
	); err != nil {
		return nil, err
	}

	return logger, nil
}

func (l *SyslogLogger) Close() error {
	if l.writer != nil {
		err := l.writer.Close()
		l.writer = nil
		return err
	}

	return nil
}

func (l *SyslogLogger) log(p syslog.Priority, args ...any) {
	item := l.pool.Get().(*[]byte)

	buf := *item
	buf = buf[:0]
	buf = append(buf, l.prefix...)
	buf = append(buf, ": "...)
	buf = fmt.Append(buf, args...)
	buf = append(buf, l.fieldsCache...)

	_ = l.writer.Write(p, buf)

	if cap(buf) <= maxBufSize {
		l.pool.Put(item)
	}
}

func (l *SyslogLogger) Print(args ...any) {
	l.log(syslog.LOG_NOTICE, args...)
}

func (l *SyslogLogger) Trace(args ...any) {
	if l.GetLevel() >= TraceLevel {
		l.log(syslog.LOG_DEBUG, args...)
	}
}

func (l *SyslogLogger) Debug(args ...any) {
	if l.GetLevel() >= DebugLevel {
		l.log(syslog.LOG_DEBUG, args...)
	}
}

func (l *SyslogLogger) Info(args ...any) {
	if l.GetLevel() >= InfoLevel {
		l.log(syslog.LOG_INFO, args...)
	}
}

func (l *SyslogLogger) Notice(args ...any) {
	if l.GetLevel() >= NoticeLevel {
		l.log(syslog.LOG_NOTICE, args...)
	}
}

func (l *SyslogLogger) Warn(args ...any) {
	if l.GetLevel() >= WarnLevel {
		l.log(syslog.LOG_WARNING, args...)
	}
}

func (l *SyslogLogger) Error(args ...any) {
	if l.GetLevel() >= ErrorLevel {
		l.log(syslog.LOG_ERR, args...)
	}
}

func (l *SyslogLogger) Fatal(args ...any) {
	if l.GetLevel() >= FatalLevel {
		l.log(syslog.LOG_CRIT, args...)
	}
}

func (l *SyslogLogger) Panic(args ...any) {
	item := l.pool.Get().(*[]byte)

	buf := *item
	buf = buf[:0]
	buf = append(buf, l.prefix...)
	buf = append(buf, ": "...)
	buf = fmt.Append(buf, args...)
	buf = append(buf, l.fieldsCache...)

	_ = l.writer.Write(syslog.LOG_CRIT, buf)

	msg := string(buf)

	if cap(buf) <= maxBufSize {
		l.pool.Put(item)
	}

	panic(msg)
}

func (l *SyslogLogger) logf(p syslog.Priority, format string, args ...any) {
	item := l.pool.Get().(*[]byte)

	buf := *item
	buf = buf[:0]
	buf = append(buf, l.prefix...)
	buf = append(buf, ": "...)
	buf = fmt.Appendf(buf, format, args...)
	buf = append(buf, l.fieldsCache...)

	_ = l.writer.Write(p, buf)

	if cap(buf) <= maxBufSize {
		l.pool.Put(item)
	}
}

func (l *SyslogLogger) Printf(format string, args ...any) {
	l.logf(syslog.LOG_NOTICE, format, args...)
}

func (l *SyslogLogger) Tracef(format string, args ...any) {
	if l.GetLevel() >= TraceLevel {
		l.logf(syslog.LOG_DEBUG, format, args...)
	}
}

func (l *SyslogLogger) Debugf(format string, args ...any) {
	if l.GetLevel() >= DebugLevel {
		l.logf(syslog.LOG_DEBUG, format, args...)
	}
}

func (l *SyslogLogger) Infof(format string, args ...any) {
	if l.GetLevel() >= InfoLevel {
		l.logf(syslog.LOG_INFO, format, args...)
	}
}

func (l *SyslogLogger) Noticef(format string, args ...any) {
	if l.GetLevel() >= NoticeLevel {
		l.logf(syslog.LOG_NOTICE, format, args...)
	}
}

func (l *SyslogLogger) Warnf(format string, args ...any) {
	if l.GetLevel() >= WarnLevel {
		l.logf(syslog.LOG_WARNING, format, args...)
	}
}

func (l *SyslogLogger) Errorf(format string, args ...any) {
	if l.GetLevel() >= ErrorLevel {
		l.logf(syslog.LOG_ERR, format, args...)
	}
}

func (l *SyslogLogger) Fatalf(format string, args ...any) {
	if l.GetLevel() >= FatalLevel {
		l.logf(syslog.LOG_CRIT, format, args...)
	}
}

func (l *SyslogLogger) Panicf(format string, args ...any) {
	item := l.pool.Get().(*[]byte)

	buf := *item
	buf = buf[:0]
	buf = append(buf, l.prefix...)
	buf = append(buf, ": "...)
	buf = fmt.Appendf(buf, format, args...)
	buf = append(buf, l.fieldsCache...)

	_ = l.writer.Write(syslog.LOG_CRIT, buf)

	msg := string(buf)

	if cap(buf) <= maxBufSize {
		l.pool.Put(item)
	}

	panic(msg)
}

func (l *SyslogLogger) WithPrefix(prefix string) Logger {
	logger := &SyslogLogger{
		prefix:      prefix,
		fields:      l.fields,
		fieldsCache: l.fieldsCache,
		pool:        l.pool,
		writer:      l.writer,
	}
	logger.SetLevel(l.GetLevel())

	return logger
}

func (l *SyslogLogger) Prefix() string {
	return l.prefix
}

func (l *SyslogLogger) WithFields(fields Fields) Logger {
	logger := &SyslogLogger{
		prefix: l.prefix,
		fields: l.fields.WithFields(fields),
		pool:   l.pool,
		writer: l.writer,
	}
	logger.SetLevel(l.GetLevel())

	if len(fields) == 0 {
		logger.fieldsCache = l.fieldsCache
	} else {
		logger.fieldsCache = l.fieldsCache + " " + fields.String()
	}

	return logger
}

func (l *SyslogLogger) Fields() Fields {
	return l.fields
}

func (l *SyslogLogger) Elapsed(start time.Time) Logger {
	return l.WithFields(Fields{"elapsed": time.Since(start).String()})
}

func (l *SyslogLogger) GetLevel() Level {
	return Level(l.level.Load())
}

func (l *SyslogLogger) SetLevel(level Level) Logger {
	if level > TraceLevel {
		level = TraceLevel
	}

	l.level.Store(uint32(level))

	return l
}
