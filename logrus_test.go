package log_test

import (
	"testing"

	"gitlab.com/miatel/go/log"
)

func TestLogrusLogger(t *testing.T) {
	RunSuite(t, func() (log.Logger, error) {
		return log.NewDefaultLogrusLogger()
	})
}
