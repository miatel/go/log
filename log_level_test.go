package log_test

import (
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/miatel/go/log"
)

type LevelSuite struct {
	suite.Suite
}

func TestLevel(t *testing.T) {
	suite.Run(t, new(LevelSuite))
}

func (v *LevelSuite) TestToString() {
	testCases := []struct {
		Name         string
		Value        log.Level
		ResultString string
	}{
		{
			Name:         "panic",
			Value:        log.PanicLevel,
			ResultString: "panic",
		},
		{
			Name:         "fatal",
			Value:        log.FatalLevel,
			ResultString: "fatal",
		},
		{
			Name:         "error",
			Value:        log.ErrorLevel,
			ResultString: "error",
		},
		{
			Name:         "warning",
			Value:        log.WarnLevel,
			ResultString: "warning",
		},
		{
			Name:         "notice",
			Value:        log.NoticeLevel,
			ResultString: "notice",
		},
		{
			Name:         "info",
			Value:        log.InfoLevel,
			ResultString: "info",
		},
		{
			Name:         "debug",
			Value:        log.DebugLevel,
			ResultString: "debug",
		},
		{
			Name:         "trace",
			Value:        log.TraceLevel,
			ResultString: "trace",
		},
		{
			Name:         "unknown",
			Value:        log.Level(9999),
			ResultString: "unknown",
		},
	}

	for _, test := range testCases {
		v.Run(test.Name, func() {
			v.Require().Equal(test.ResultString, test.Value.String())
		})
	}
}

func (v *LevelSuite) TestUnmarshalText() {
	testCases := []struct {
		Name        string
		Value       []byte
		ResultLevel log.Level
		Result      bool
	}{
		{
			Name:        "panic",
			Value:       []byte("panic"),
			ResultLevel: log.PanicLevel,
			Result:      true,
		},
		{
			Name:        "fatal",
			Value:       []byte("fatal"),
			ResultLevel: log.FatalLevel,
			Result:      true,
		},
		{
			Name:        "error",
			Value:       []byte("error"),
			ResultLevel: log.ErrorLevel,
			Result:      true,
		},
		{
			Name:        "warning",
			Value:       []byte("warning"),
			ResultLevel: log.WarnLevel,
			Result:      true,
		},
		{
			Name:        "notice",
			Value:       []byte("notice"),
			ResultLevel: log.NoticeLevel,
			Result:      true,
		},
		{
			Name:        "info",
			Value:       []byte("info"),
			ResultLevel: log.InfoLevel,
			Result:      true,
		},
		{
			Name:        "debug",
			Value:       []byte("debug"),
			ResultLevel: log.DebugLevel,
			Result:      true,
		},
		{
			Name:        "trace",
			Value:       []byte("trace"),
			ResultLevel: log.TraceLevel,
			Result:      true,
		},
		{
			Name:        "unknown",
			Value:       []byte("unknown"),
			ResultLevel: log.Level(0),
			Result:      false,
		},
	}

	for _, test := range testCases {
		v.Run(test.Name, func() {
			var level log.Level
			err := level.UnmarshalText(test.Value)
			if test.Result {
				v.Require().NoError(err)
				v.Require().Equal(test.ResultLevel, level)
			} else {
				v.Require().Error(err)
				v.Require().Equal(test.ResultLevel, level)
			}
		})
	}
}
