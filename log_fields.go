package log

import (
	"fmt"

	"golang.org/x/exp/slices"
)

type Fields map[string]any

func (v Fields) String() string {
	if len(v) == 0 {
		return ""
	}

	keys := make([]string, 0, 32)
	for key := range v {
		keys = append(keys, key)
	}

	slices.Sort(keys)

	b := make([]byte, 0, 512)
	for _, key := range keys {
		b = append(b, key...)
		b = append(b, '=')
		b = fmt.Append(b, v[key])
		b = append(b, ' ')
	}
	b = b[:len(b)-1]

	return string(b)
}

func (v Fields) WithFields(new Fields) Fields {
	all := make(Fields, len(v)+len(new))

	for key, val := range v {
		all[key] = val
	}

	for key, val := range new {
		all[key] = val
	}

	return all
}
