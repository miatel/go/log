package log

import (
	"sync/atomic"
	"time"

	"github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
)

type LogrusLogger struct {
	level  atomic.Uint32
	prefix string
	fields Fields
	logrus *logrus.Logger
}

func NewDefaultLogrusLogger() (*LogrusLogger, error) {
	logger := &LogrusLogger{
		prefix: "main",
		logrus: logrus.New(),
	}
	logger.SetLevel(InfoLevel)

	logger.logrus.Formatter = &prefixed.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02 15:04:05.000",
	}
	logger.logrus.ExitFunc = func(int) {}
	logger.logrus.SetLevel(logrus.TraceLevel)

	return logger, nil
}

func (l *LogrusLogger) Close() error {
	return nil
}

func (l *LogrusLogger) prepareEntry() *logrus.Entry {
	return l.logrus.
		WithFields(logrus.Fields(l.Fields())).
		WithField("prefix", l.Prefix())
}

func (l *LogrusLogger) Print(args ...any) {
	l.prepareEntry().Print(args...)
}

func (l *LogrusLogger) Trace(args ...any) {
	if l.GetLevel() >= TraceLevel {
		l.prepareEntry().Trace(args...)
	}
}

func (l *LogrusLogger) Debug(args ...any) {
	if l.GetLevel() >= DebugLevel {
		l.prepareEntry().Debug(args...)
	}
}

func (l *LogrusLogger) Info(args ...any) {
	if l.GetLevel() >= InfoLevel {
		l.prepareEntry().Info(args...)
	}
}

func (l *LogrusLogger) Notice(args ...any) {
	if l.GetLevel() >= NoticeLevel {
		l.prepareEntry().Info(args...)
	}
}

func (l *LogrusLogger) Warn(args ...any) {
	if l.GetLevel() >= WarnLevel {
		l.prepareEntry().Warn(args...)
	}
}

func (l *LogrusLogger) Error(args ...any) {
	if l.GetLevel() >= ErrorLevel {
		l.prepareEntry().Error(args...)
	}
}

func (l *LogrusLogger) Fatal(args ...any) {
	if l.GetLevel() >= FatalLevel {
		l.prepareEntry().Fatal(args...)
	}
}

func (l *LogrusLogger) Panic(args ...any) {
	l.prepareEntry().Panic(args...)
}

func (l *LogrusLogger) Printf(format string, args ...any) {
	l.prepareEntry().Printf(format, args...)
}

func (l *LogrusLogger) Tracef(format string, args ...any) {
	if l.GetLevel() >= TraceLevel {
		l.prepareEntry().Tracef(format, args...)
	}
}

func (l *LogrusLogger) Debugf(format string, args ...any) {
	if l.GetLevel() >= DebugLevel {
		l.prepareEntry().Debugf(format, args...)
	}
}

func (l *LogrusLogger) Infof(format string, args ...any) {
	if l.GetLevel() >= InfoLevel {
		l.prepareEntry().Infof(format, args...)
	}
}

func (l *LogrusLogger) Noticef(format string, args ...any) {
	if l.GetLevel() >= NoticeLevel {
		l.prepareEntry().Infof(format, args...)
	}
}

func (l *LogrusLogger) Warnf(format string, args ...any) {
	if l.GetLevel() >= WarnLevel {
		l.prepareEntry().Warnf(format, args...)
	}
}

func (l *LogrusLogger) Errorf(format string, args ...any) {
	if l.GetLevel() >= ErrorLevel {
		l.prepareEntry().Errorf(format, args...)
	}
}

func (l *LogrusLogger) Fatalf(format string, args ...any) {
	if l.GetLevel() >= FatalLevel {
		l.prepareEntry().Fatalf(format, args...)
	}
}

func (l *LogrusLogger) Panicf(format string, args ...any) {
	l.prepareEntry().Panicf(format, args...)
}

func (l *LogrusLogger) WithPrefix(prefix string) Logger {
	logger := &LogrusLogger{
		logrus: l.logrus,
		prefix: prefix,
		fields: l.fields,
	}
	logger.SetLevel(l.GetLevel())

	return logger
}

func (l *LogrusLogger) Prefix() string {
	return l.prefix
}

func (l *LogrusLogger) WithFields(fields Fields) Logger {
	logger := &LogrusLogger{
		logrus: l.logrus,
		prefix: l.prefix,
		fields: l.fields.WithFields(fields),
	}
	logger.SetLevel(l.GetLevel())

	return logger
}

func (l *LogrusLogger) Fields() Fields {
	return l.fields
}

func (l *LogrusLogger) Elapsed(start time.Time) Logger {
	return l.WithFields(Fields{"elapsed": time.Since(start).String()})
}

func (l *LogrusLogger) GetLevel() Level {
	return Level(l.level.Load())
}

func (l *LogrusLogger) SetLevel(level Level) Logger {
	if level > TraceLevel {
		level = TraceLevel
	}

	l.level.Store(uint32(level))

	return l
}
