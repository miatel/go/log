package log_test

import (
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/miatel/go/log"
)

type FieldsSuite struct {
	suite.Suite
}

func TestFields(t *testing.T) {
	suite.Run(t, new(FieldsSuite))
}

var fieldsTestCases = []struct {
	Name         string
	Value        log.Fields
	ResultString string
	Result       bool
}{
	{
		Name:         "Nil",
		Value:        nil,
		ResultString: "",
		Result:       true,
	},
	{
		Name:         "Empty",
		Value:        log.Fields{},
		ResultString: "",
		Result:       true,
	},
	{
		Name: "Two",
		Value: log.Fields{
			"field2": "field2",
			"field1": "field1",
		},
		ResultString: "field1=field1 field2=field2",
		Result:       true,
	},
	{
		Name: "Two (wrong order)",
		Value: log.Fields{
			"field2": "field2",
			"field1": "field1",
		},
		ResultString: "field2=field2 field1=field1",
		Result:       false,
	},
	{
		Name: "Three",
		Value: log.Fields{
			"field1": "field1",
			"field2": "field2",
			"xxx":    "xxx",
		},
		ResultString: "field1=field1 field2=field2 yyy=yyy",
		Result:       false,
	},
}

func (v *FieldsSuite) TestString() {
	for _, test := range fieldsTestCases {
		v.Run(test.Name, func() {
			if test.Result {
				v.Require().Equal(test.ResultString, test.Value.String())
			} else {
				v.Require().NotEqual(test.ResultString, test.Value.String())
			}
		})
	}
}

func (v *FieldsSuite) TestWithFields() {
	for _, test := range fieldsTestCases {
		v.Run(test.Name, func() {
			test.Value = test.Value.WithFields(log.Fields{"zzz": "zzz"})
			if test.ResultString == "" {
				test.ResultString = "zzz=zzz"
			} else {
				test.ResultString = test.ResultString + " zzz=zzz"
			}

			if test.Result {
				v.Require().Equal(test.ResultString, test.Value.String())
			} else {
				v.Require().NotEqual(test.ResultString, test.Value.String())
			}
		})
	}
}

func BenchmarkFields_String(b *testing.B) {
	testCases := []struct {
		Name  string
		Value log.Fields
	}{
		{
			Name: "Two fields",
			Value: log.Fields{
				"field2": "field2",
				"field1": "field1",
			},
		},
		{
			Name: "Three fields",
			Value: log.Fields{
				"field3": "field3",
				"field2": "field2",
				"field1": "field1",
			},
		},
		{
			Name: "Four fields",
			Value: log.Fields{
				"field4": 10,
				"field3": "field3",
				"field2": "field2",
				"field1": "field1",
			},
		},
	}

	for _, test := range testCases {
		b.Run(test.Name, func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				_ = test.Value.String()
			}
		})
	}
}
