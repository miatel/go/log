package syslog

import (
	"errors"
	"net"
)

// unixConnect opens a connection to the syslog daemon running on the
// local machine using a Unix domain socket.

func unixConnect() (net.Conn, error) {
	logTypes := []string{"unixgram", "unix"}
	logPaths := []string{"/var/run/log", "/var/run/syslog", "/dev/log"}
	for _, network := range logTypes {
		for _, path := range logPaths {
			conn, err := net.Dial(network, path)
			if err == nil {
				return conn, nil
			}
		}
	}

	return nil, errors.New("syslog: unix syslog delivery error")
}
