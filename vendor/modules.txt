# github.com/davecgh/go-spew v1.1.1
## explicit
github.com/davecgh/go-spew/spew
# github.com/mattn/go-colorable v0.1.13
## explicit; go 1.15
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.20
## explicit; go 1.15
github.com/mattn/go-isatty
# github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d
## explicit
github.com/mgutz/ansi
# github.com/onsi/ginkgo v1.16.5
## explicit; go 1.16
# github.com/onsi/gomega v1.17.0
## explicit; go 1.16
# github.com/pmezard/go-difflib v1.0.0
## explicit
github.com/pmezard/go-difflib/difflib
# github.com/sirupsen/logrus v1.9.3
## explicit; go 1.13
github.com/sirupsen/logrus
# github.com/stretchr/testify v1.8.4
## explicit; go 1.20
github.com/stretchr/testify/assert
github.com/stretchr/testify/require
github.com/stretchr/testify/suite
# github.com/x-cray/logrus-prefixed-formatter v0.5.2
## explicit
github.com/x-cray/logrus-prefixed-formatter
# gitlab.com/miatel/go/syslog v1.0.4
## explicit; go 1.20
gitlab.com/miatel/go/syslog
# golang.org/x/crypto v0.17.0
## explicit; go 1.18
golang.org/x/crypto/ssh/terminal
# golang.org/x/exp v0.0.0-20231226003508-02704c960a9b
## explicit; go 1.20
golang.org/x/exp/constraints
golang.org/x/exp/slices
# golang.org/x/sys v0.15.0
## explicit; go 1.18
golang.org/x/sys/plan9
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/term v0.15.0
## explicit; go 1.18
golang.org/x/term
# gopkg.in/yaml.v3 v3.0.1
## explicit
gopkg.in/yaml.v3
