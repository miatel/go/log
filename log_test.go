package log_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	"gitlab.com/miatel/go/log"
)

type LogSuite struct {
	suite.Suite
	logger    log.Logger
	newLogger NewLogger
}

type NewLogger func() (log.Logger, error)

func RunSuite(t *testing.T, newLogger NewLogger) {
	v := &LogSuite{
		newLogger: newLogger,
	}

	suite.Run(t, v)
}

func (v *LogSuite) SetupTest() {
	logger, err := v.newLogger()
	v.Require().Nil(err)
	v.logger = logger
}

func (v *LogSuite) TearDownTest() {
	v.Logger().Close()
	v.logger = nil
}

func (v *LogSuite) Logger() log.Logger {
	return v.logger
}

func (v *LogSuite) TestNew() {
	v.Require().Equal("main", v.Logger().Prefix())
	v.Require().Equal(log.InfoLevel, v.Logger().GetLevel())
	v.Require().Equal(log.Fields(nil), v.Logger().Fields())
}

func (v *LogSuite) TestWithPrefix() {
	v.Logger().SetLevel(log.TraceLevel)

	newLogger := v.Logger().WithPrefix("test")
	v.Require().NotSame(v.Logger(), newLogger)
	v.Require().Equal("test", newLogger.Prefix())
	v.Require().Equal(log.TraceLevel, v.Logger().GetLevel())
	v.Require().Equal(log.TraceLevel, newLogger.GetLevel())

	sameLogger := newLogger.SetLevel(log.DebugLevel)
	v.Require().Same(newLogger, sameLogger)
	v.Require().Equal(log.TraceLevel, v.Logger().GetLevel())
	v.Require().Equal(log.DebugLevel, sameLogger.GetLevel())
}

func (v *LogSuite) TestWithFields() {
	testCases := []struct {
		Name         string
		Value        log.Fields
		ResultFields log.Fields
	}{
		{
			Name:         "Nil",
			Value:        nil,
			ResultFields: log.Fields{},
		},
		{
			Name:         "Empty",
			Value:        log.Fields{},
			ResultFields: log.Fields{},
		},
		{
			Name: "Two",
			Value: log.Fields{
				"field1": "value1",
				"field2": "value2",
			},
			ResultFields: log.Fields{
				"field1": "value1",
				"field2": "value2",
			},
		},
	}

	for _, test := range testCases {
		v.Run(test.Name, func() {
			v.Logger().SetLevel(log.TraceLevel)

			newLogger := v.Logger().WithFields(test.Value)
			v.Require().NotSame(v.Logger(), newLogger)
			v.Require().Equal(test.ResultFields, newLogger.Fields())
			v.Require().Equal(log.TraceLevel, v.Logger().GetLevel())
			v.Require().Equal(log.TraceLevel, newLogger.GetLevel())

			sameLogger := newLogger.SetLevel(log.DebugLevel)
			v.Require().Same(newLogger, sameLogger)
			v.Require().Equal(log.TraceLevel, v.Logger().GetLevel())
			v.Require().Equal(log.DebugLevel, sameLogger.GetLevel())
		})
	}
}

func (v *LogSuite) TestElapsed() {
	logger := v.Logger().Elapsed(time.Now())
	v.Require().Contains(logger.Fields().String(), "elapsed=")
}

func (v *LogSuite) TestLevel() {
	v.Run("Set", func() {
		sameLogger := v.Logger().SetLevel(log.DebugLevel)
		v.Require().Same(v.Logger(), sameLogger)
		v.Require().Equal(log.DebugLevel, v.Logger().GetLevel())
	})

	v.Run("Set unknown", func() {
		sameLogger := v.Logger().SetLevel(9999)
		v.Require().Same(v.Logger(), sameLogger)
		v.Require().Equal(log.TraceLevel, sameLogger.GetLevel())
	})
}

func (v *LogSuite) TestLog() {
	v.Logger().SetLevel(log.TraceLevel)

	v.Logger().Print("test")
	v.Logger().Trace("test")
	v.Logger().Debug("test")
	v.Logger().Info("test")
	v.Logger().Notice("test")
	v.Logger().Warn("test")
	v.Logger().Error("test")
	v.Logger().Fatal("test")

	defer func() {
		if err := recover(); err == nil {
			v.Fail("Panic not occurred")
		}
	}()
	v.Logger().Panic("test")
}

func (v *LogSuite) TestLogf() {
	v.Logger().SetLevel(log.TraceLevel)

	v.Logger().Printf("test %s", "test")
	v.Logger().Tracef("test %s", "test")
	v.Logger().Debugf("test %s", "test")
	v.Logger().Infof("test %s", "test")
	v.Logger().Noticef("test %s", "test")
	v.Logger().Warnf("test %s", "test")
	v.Logger().Errorf("test %s", "test")
	v.Logger().Fatalf("test %s", "test")

	defer func() {
		if err := recover(); err == nil {
			v.Fail("Panic not occurred")
		}
	}()
	v.Logger().Panicf("test %s", "test")
}
